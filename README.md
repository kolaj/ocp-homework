# OpenShift HA Homework Lab

This repository contains solution for 40 hours Openshift Homework task.

This instalation procedure fulfills requirements given in the instruction:

#### Basic Requirements:
* Ability to authenticate at the master console - define in [templates/hosts_template](templates/hosts_template) file at lines 101-102   
  User admin is a cluster-admin.  
  All users,admin jenkins Andrew Amy Brian Betty, use the same password `redhat1`

* Registry has storage attached and working - define in [templates/hosts_template](templates/hosts_template) file at lines 208-213

* Router is configured on each infranode - define in [templates/hosts_template](templates/hosts_template) file at line 198

* PVs of different types are available for users to consume - define in [scripts/pv.sh](scripts/pv.sh) script

#### HA Requirements:

All are define in [templates/hosts_template](templates/hosts_template) file:

* There are three masters working - at lines 246-249

* There are three etcd instances working - at lines 251-254

* There is a load balancer to access the masters called loadbalancer.$GUID.$DOMAIN - at line 71

* There is a load balancer/DNS for both infranodes called *.apps.$GUID.$DOMAIN - at line 72

* There are at least two infranodes, in node_group node-config-infra labeled node-role.kubernetes.io/infra=true' - at lines 263-264

#### Environment Configuration:

All are define in [templates/hosts_template](templates/hosts_template) file:

* NetworkPolicy is configured and working with projects isolated by default - at line 92

* Aggregated logging is configured and working

* Metrics collection is configured and working

* Router and Registry Pods run on Infranodes - at lines 195-196

* Metrics and Logging components run on Infranodes - at lines 138-140 and 185-188

* Service Catalog, Template Service Broker, and Ansible Service Broker are all working at lines 220,223,227

#### CICD Workflow:

* Jenkins pod is running in a project called tasks-build with a persistent volume - define in [scripts/cicd.sh](scripts/cicd.sh) script

* Jenkins deploys openshift-tasks app - define in scripts/cicd/sh and and [templates/jenkins.yml](templates/jenkins.yml) file

* Jenkins OpenShift plugin is used in the buildconfig to create a CICD workflow - define in [templates/jenkins.yml](templates/jenkins.yml) file

* Use tasks-build as your project for building. Do promotion between projects tasks-dev, tasks-test, and tasks-prod as your project names. - define in [scripts/pipeline.sh](scripts/pipeline.sh) and [templates/jenkins.yml](templates/jenkins.yml) file

* HPA called tasks-hpa is configured and working on production deployment of openshift-tasks in project tasks-prod  - define in [scripts/pipeline.sh](scripts/pipeline.sh) script

#### Multitenancy:

All are define in [scripts/multitenancy.sh](scripts/multitenancy.sh)

* Multiple Clients (customers) created

* Clients will be named Alpha Corp and Beta Corp (client=alpha, client=beta), and a "client=common" for unspecified customers.

* Alpha Corp will have two users, Amy and Andrew

* Beta Corp will have two users, Brian and Betty

* Common will be for all other users workloads

* Dedicated node for each Client

* The new project template is modified so that it includes a LimitRange

* A new user template is used to create a user object with the specific label value

* Alpha and Beta Corp users are confined to projects, and all new pods are deployed to customer dedicated nodes


### Installation instructions

1. Login on Bastion host `$ ssh -i ~/.ssh/"private_key" username@bastion.$GUID.example.opentlc.com`
2. Switch to root user `sudo -i`
3. Clone this repository  `git clone https://gitlab.com/kolaj/ocp-homework.git`
4. Go to ocp-homework directory `cd ocp-homework`
5. Execute install.py script `./install.py`
6. Whole installattion may take up to 40-50 minutes.
7. After instalation is done, addresses to master-console, jenkins instance and applications from dev,test and prod environment will be shown this way:
```
##########  Openshift console address: https://loadbalancer.$GUID.example.opentlc.com
##########  Jenkins with pipeline output from $build namespace: https://jenkins-$build.apps.$GUID.example.opentlc.com/job/$build/job/$build-pipeline/
##########  Application from dev environment: https://$app_name-$dev.apps.$GUID.example.opentlc.com 
##########  Application from test environmnet: https://$app_name-$test.apps.$GUID.example.opentlc.com
##########  Application from prod environmnet: https://$app_name-$prod.apps.$GUID.example.opentlc.com
```

### Credits

Pipeline is based on DevOps with OpenShift, Chapter 4 - https://www.openshift.com/devops-with-openshift/  
Scripts responsible for creating PVs are copied from https://www.opentlc.com/labs/ocp_advanced_deployment/02_1_HA_Deployment_Solution_Lab.html
