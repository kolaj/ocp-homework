#!/usr/bin/python3.4

import os
import subprocess
import time

os.environ['GUID'] = os.uname()[1].split('.')[1]
subprocess.call(["echo $GUID"], shell=True)

print ("### Update guid value in hosts file")
subprocess.call(["/usr/bin/bash", "/root/ocp-homework/scripts/generate_inventory.sh"])
print ("### Install ansible and rest of dependencies if needed")
subprocess.call(["yum", "install", "python-pip", "openshift-ansible", "atomic-openshift-clients", "-y"])
subprocess.call(["pip", "install", "ansible==2.6.5"])
print ("### Installation done")

subprocess.call(["ansible-playbook", "/usr/share/ansible/openshift-ansible/playbooks/prerequisites.yml"])
subprocess.call(["ansible-playbook", "/usr/share/ansible/openshift-ansible/playbooks/deploy_cluster.yml"])

print("### Once the installation is complete, copy the .kube directory from master1 to your bastion host")
subprocess.call(["ansible masters[0] -b -m fetch -a \"src=/root/.kube/config dest=/root/.kube/config flat=yes\""],shell=True)

print("### Verify that you are now system:admin")
subprocess.call(["oc whoami"],shell=True)

print("#### Get node list")
subprocess.call(["oc get nodes"],shell=True)


print("### Add admin user cluster-admin role")
subprocess.call(["oc adm policy add-cluster-role-to-user cluster-admin admin"],shell=True)


print("### Create pvs folders")

subprocess.call(["ansible nfs -b -m copy -a \"src=/root/ocp-homework/scripts/pv.sh dest=/tmp/pv.sh\""],shell=True)
subprocess.call(["ansible nfs -b -m shell -a \"/bin/sh /tmp/pv.sh\""],shell=True)

print("### Create 25pvs 5 gb")
subprocess.call(["/usr/bin/bash", "/root/ocp-homework/scripts/5gb.sh"])
print("### Create 25pvs 10 gb")
subprocess.call(["/usr/bin/bash", "/root/ocp-homework/scripts/10gb.sh"])

print("### Create pvs from yaml files")
subprocess.call(["cat /root/pvs/* | oc create -f -"],shell=True)

print("### Update default project template with limits")
subprocess.call(["oc create -f /root/ocp-homework/templates/project-req.yaml -n default"],shell=True)

print("### Update master-config.yaml")
subprocess.call(["ansible masters -m lineinfile -a 'path=/etc/origin/master/master-config.yaml regexp=\"projectRequestTemplate\" line=\"  projectRequestTemplate: default/project-request\"'"],shell=True)

print("### Restart the appropriate control-plane pods to pick up the changes")
subprocess.call(["ansible masters -m shell -a '/usr/local/bin/master-restart api; /usr/local/bin/master-restart controllers'"],shell=True)

print("### 20 sec sleep, to wait for api and controllers after restart")
time.sleep(20)


print("### Setup ci-cd environment")
subprocess.call(["/usr/bin/bash", "/root/ocp-homework/scripts/cicd.sh"])


print("### Waiting for jenkins pod")
while True:
  p = subprocess.Popen('oc get pod -n tasks-build -o=wide | grep jenkins | grep -v build | grep -v deploy | grep \'1/1.* Running\'', stdout=subprocess.PIPE, shell=True, universal_newlines=True)
  result = p.communicate()[0]
  if result == "":
     print("Pod is not alive, waiting 10 sec")
     time.sleep(10)
  else:
     print("Pod is alive")
     print (result)
     break


print("### Configure and start pipeline")
subprocess.call(["/usr/bin/bash", "/root/ocp-homework/scripts/pipeline.sh"])


print("### Create projects and groups for users Amy Andrew Brian Betty")
subprocess.call(["/usr/bin/bash", "/root/ocp-homework/scripts/multitenancy.sh"])
