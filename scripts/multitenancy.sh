#!/usr/bin/bash
export GUID=`hostname | cut -d"." -f2`


oc adm groups new alpha Amy Andrew
oc adm groups new beta Brian Betty
oc adm groups new common
oc label node node1.$GUID.internal client=alpha
oc label node node2.$GUID.internal client=beta
oc label node node3.$GUID.internal client=common
oc adm new-project alpha-project --display-name='Alpha Corp' --node-selector='client=alpha'
oc adm new-project beta-project --display-name='Beta Corp' --node-selector='client=beta'
oc policy add-role-to-group edit beta -n beta-project
oc policy add-role-to-group edit alpha -n alpha-project
