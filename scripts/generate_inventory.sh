#!/usr/bin/env bash



echo "Copy a new hosts file to /etc/ansible/hosts"

cp /root/ocp-homework/templates/hosts_template /etc/ansible/hosts


echo "Set the current GUID to generate the inventory"

sed -i "s/GUID/$GUID/g" /etc/ansible/hosts
