#!/usr/bin/bash

export GUID=`hostname | cut -d"." -f2`
app_name=openshift-task
app_template='https://github.com/devops-with-openshift/cotd.git#master'
build=tasks-build
dev=tasks-dev
test=tasks-test
prod=tasks-prod

echo "### SETUP OPENSHIFT TO RUN PIPELINE"
oc project $dev
oc new-app --name=$app_name  openshift/php:5.6~$app_template
oc expose service $app_name --name=$app_name --hostname=$app_name-$dev.apps.$GUID.example.opentlc.com
oc patch route $app_name -p '{"spec":{"tls":{"termination":"edge"}}}'

oc project $test
oc create dc $app_name --image=docker-registry.default.svc:5000/$dev/$app_name:promoteQA
oc patch dc/$app_name -p '{"spec":{"template":{"spec":{"containers":[{"name":"default-container","imagePullPolicy":"Always"}]}}}}'
oc scale dc/$app_name --replicas=0
oc expose dc $app_name --port=8080
oc expose service $app_name --name=$app_name --hostname=$app_name-$test.apps.$GUID.example.opentlc.com
oc patch route $app_name -p '{"spec":{"tls":{"termination":"edge"}}}'

oc project $prod
oc create dc $app_name --image=docker-registry.default.svc:5000/$dev/$app_name:promotePRD
oc patch dc/$app_name -p '{"spec":{"template":{"spec":{"containers":[{"name":"default-container","imagePullPolicy":"Always"}]}}}}'
oc scale dc/$app_name --replicas=0
oc expose dc $app_name --port=8080
oc expose service $app_name --name=$app_name --hostname=$app_name-$prod.apps.$GUID.example.opentlc.com
oc patch route $app_name -p '{"spec":{"tls":{"termination":"edge"}}}'


echo "### Add HPA to $prod namespace"
oc autoscale dc/$app_name --min 1 --max 10 --cpu-percent=90 -n $prod
echo "### RUN PIPELINE"
oc start-build pipeline -n $build

echo "##########  Openshift console address: https://loadbalancer.$GUID.example.opentlc.com
##########  Jenkins with pipeline output from $build namespace: https://jenkins-$build.apps.$GUID.example.opentlc.com/job/$build/job/$build-pipeline/
##########  Application from dev environment: https://$app_name-$dev.apps.$GUID.example.opentlc.com 
##########  Application from test environmnet: https://$app_name-$test.apps.$GUID.example.opentlc.com
##########  Application from prod environmnet: https://$app_name-$prod.apps.$GUID.example.opentlc.com "
      
