#!/usr/bin/bash

echo "######### Create new projects for pipeline"
oc new-project tasks-build --display-name='CICD Jenkins' --description='CICD Jenkins'
oc new-project tasks-dev --display-name='Development' --description='Development'
oc new-project tasks-test --display-name='Testing' --description='Testing'
oc new-project tasks-prod --display-name='Production' --description='Production'


oc new-app -f /root/ocp-homework/templates/jenkins-pre.yaml -n tasks-build
oc create -f /root/ocp-homework/templates/jenkins.yml -n tasks-build

echo "##### ADD JENKINS USER PERMISSIONS TO SERVICEACCOUNT"
oc policy add-role-to-user edit system:serviceaccount:tasks-build:jenkins -n tasks-dev
oc policy add-role-to-user edit system:serviceaccount:tasks-build:jenkins -n tasks-test
oc policy add-role-to-user edit system:serviceaccount:tasks-build:jenkins -n tasks-prod

oc policy add-role-to-user system:image-puller system:serviceaccount:tasks-prod:default -n tasks-dev
oc policy add-role-to-user system:image-puller system:serviceaccount:tasks-test:default -n tasks-dev
